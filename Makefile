##
## Makefile for MegaBoot
##
## Made by Pierre Surply
## <pierre.surply@gmail.com>
##
## Started on  Sat Jun 15 18:33:51 2013 Pierre Surply
## Last update Sun Feb 23 14:38:28 2014 Pierre Surply
##

PROJ	= megaboot
MCU	= 328p
DEV	= /dev/ttyU0

CC	= avr-gcc
CFLAGS	= -Wall -mmcu=atmega$(MCU) -DF_CPU=16000000L  \
	  -Os -fno-inline-small-functions -fno-split-wide-types
LDFLAGS	= -Wl,--section-start=.text=0x7900 -Wl,--relax -nostartfiles

OBJCOPY	= avr-objcopy
OBJDUMP	= avr-objdump
SIZE	= avr-size

AVRDUDE	= avrdude
PARTNO	= m$(MCU)
PROG	= stk500v1 -b 19200
AFLAGS	= -p $(PARTNO) -c $(PROG) -P $(DEV) -e
FUSE	= -U hfuse:w:0xD8:m

ELF	= $(PROJ).elf
HEX	= $(PROJ).hex

SRC	= main.c utils.c uart.c stk500.c watchdog.c shell.c boot.c
OBJ	= $(SRC:.c=.o)

all: $(HEX)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

%.elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

%.hex: %.elf
	$(SIZE) -A $<
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

upload: $(HEX)
	$(AVRDUDE) $(AFLAGS) -U "flash:w:$<" $(FUSE)

test::
	$(MAKE) -C ./test upload

test-erase::
	$(MAKE) -C ./test erase

shell:
	chmod a+rw $(DEV)
	minicom -w -D $(DEV)

clean:
	rm -rf *.o *.elf *.hex
