# MegaBoot

MegaBoot is a bootloader for Arduino. It's fully compatible with the original
Arduino bootloader and provides some extra features.

It allows the programming of the microcontroller with *avrdude* or *Arduino IDE*.

    avrdude -v -p m328p -c arduino -b 115200 -P <device> ...

## Commands

The bootloader can be controlled via command line interface through UART
(use *minicom* or an other serial communication program).

The MegaBoot shell is run when the application flash section is empty or if
A0 pin is wired with the 5V pin during the MCU reset.

  - **start** : tries to start the application.
  - **info** : shows information about the device.
  - **erase** : erases the application flash section.

## Recommended fuses configuration

### Arduino UNO (`Atmega328p`)

  - Low fuse : `0xFF`
  - High fuse : `0xD8`
  - Ext fuse : `05`

## Contact

  - Pierre Surply (pierre.surply@gmail.com)