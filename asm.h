/*
** asm.h for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Jun 15 19:08:55 2013 Pierre Surply
** Last update Sat Jun 15 22:22:37 2013 Pierre Surply
*/

#ifndef _ASM_H_
#define _ASM_H_

#define cli()   asm volatile("cli\n\t"::)
#define sei()   asm volatile("sei\n\t"::)

#endif /* _ASM_H_ */
