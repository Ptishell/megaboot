/*
** boot.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Jun 15 18:59:35 2013 Pierre Surply
** Last update Fri Jun 21 17:33:13 2013 Pierre Surply
*/

#include "boot.h"
#include "asm.h"

#include <stdlib.h>
#include <avr/io.h>

#ifdef __AVR_ATmega328P__
#define SPMEN           SELFPRGEN
#define SIGRD        5
#endif

#define PAGE_ERASE      (1 << PGERS) | (1 << SPMEN)
#define PAGE_WRITE      (1 << PGWRT) | (1 << SPMEN)
#define EN_RWW          (1 << RWWSRE) | (1 << SPMEN)
#define LOCK_BITS_SET   (1 << BLBSET) | (1 << SPMEN)
#define SIG_READ        (1 << SIGRD) | (1 << SPMEN)
#define EN_SPM          (1 << SPMEN)

static void do_spm(uint8_t spmcrval, uint16_t addr, uint16_t data)
{
        while ((SPMCSR >> SPMEN) & 1);
        while ((EECR >> EEPE) & 1);
        asm volatile ("movw     r0, %0" "\n\t"
                      "out      %1, %2" "\n\t"
                      "spm"             "\n\t"
                      "clr      r1"     "\n\t"
                      :
                      : "r" (data),
                        "i" (_SFR_IO_ADDR(SPMCSR)),
                        "r" (spmcrval),
                        "z" (addr));
}

static void write_buffer(uint8_t *src, size_t dst)
{
        size_t  i, d;

        i = SPM_PAGESIZE / 2;
        do
        {
                d = *(src++);
                d |= (*(src++)) << 8;
                do_spm(EN_SPM, dst, d);
                dst += 2;
        } while (--i);
}

void write_page(void *src, size_t dst)
{
        do_spm(PAGE_ERASE, dst, 0);

        write_buffer(src, dst);

        do_spm(PAGE_WRITE, dst, 0);

        do_spm(EN_RWW, 0, 0);
}

void set_erased(void)
{
        uint8_t         buff[SPM_PAGESIZE];

        buff[0] = 0xFF;
        write_page(buff, 0);
}

uint8_t read_fuse(uint16_t fuse)
{
        uint8_t res;

        asm volatile ("out      %1, %2" "\n\t"
                      "lpm      %0, Z"  "\n\t"
                      : "=r" (res)
                      : "i" (_SFR_IO_ADDR(SPMCSR)),
                        "r" (LOCK_BITS_SET),
                        "z" (fuse));

        return res;
}


uint8_t read_sign(uint16_t addr)
{
        uint8_t res;

        asm volatile ("out %1, %2"      "\n\t"
                      "lpm %0, Z"        "\n\t"
                      : "=r" (res)
                      : "i" (_SFR_IO_ADDR(SPMCSR)),
                        "r" (SIG_READ),
                        "z" (addr));

        return res;
}
