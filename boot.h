/*
** boot.h for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Jun 15 19:38:49 2013 Pierre Surply
** Last update Fri Jun 21 17:32:57 2013 Pierre Surply
*/

#ifndef _BOOT_H_
#define _BOOT_H_

#include <stdlib.h>
#include <inttypes.h>

#define ADDR_LFUSE      0
#define ADDR_LOCK       1
#define ADDR_EFUSE      2
#define ADDR_HFUSE      3

#define SIGN_BYTE1      0
#define SIGN_BYTE2      2
#define SIGN_BYTE3      4
#define RC_CALIB        1

void    write_page(void *src, size_t dst);
uint8_t read_fuse(uint16_t fuse);
void    set_erased(void);
uint8_t read_sign(uint16_t addr);

#endif /* _BOOT_H_ */
