/*
** main.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Jun 15 18:33:29 2013 Pierre Surply
** Last update Sun Feb 23 14:39:48 2014 Pierre Surply
*/

#include "megaboot.h"
#include "stk500.h"
#include "watchdog.h"
#include "uart.h"
#include "shell.h"
#include "asm.h"

#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

const char pgm_starting[]     PROGMEM = "Starting application...\n\r";

void jmpmain(void) __attribute__ ((naked))
        __attribute__ ((section (".init9")));

void start_app(void)
{
        wdc(WATCHDOG_OFF);
        if (pgm_read_byte_near(0) == 0xFF)
                shell();
        else
        {
                welcome();
                print(pgm_starting);
                asm volatile ("clr r30\n\t"
                              "clr r31\n\t"
                              "ijmp\n\t"::);
        }
}

void check_hw_reset(void)
{
       uint8_t tmp;

        tmp = MCUSR;
        asm volatile ("out %0, r1"
                      :
                      : "i" (_SFR_IO_ADDR(MCUSR)));
        if (!(tmp & (1 << EXTRF)))
                start_app();
}


void jmpmain(void)
{
        asm volatile ( ".set __stack, %0"
                       :
                       : "i" (RAMEND) );
        asm volatile ( "clr __zero_reg__" );
        asm volatile ( "rjmp main");
}

int main(void)
{
        cli();
        DDRB = 0xFF;
        DDRC = 0;
        PORTB = 0;
        uart_init();
        if (PINC & 1)
                shell();
        wdc(WATCHDOG_1S);
        wdr();
        check_hw_reset();
        stk500_loop();
        return 0;
}
