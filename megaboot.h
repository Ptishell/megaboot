/*
** megaboot.h for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Jun 17 11:11:05 2013 Pierre Surply
** Last update Fri Jun 21 13:49:18 2013 Pierre Surply
*/

#ifndef _MEGABOOT_H_
#define _MEGABOOT_H_

#define SW_MAJOR        1
#define SW_MINOR        0
#define VTARGET         50
#define AREF            50

void start_app(void) __attribute__ ((naked));

#endif /* _MEGABOOT_H_ */
