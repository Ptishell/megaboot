/*
** shell.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Thu Jun 20 17:22:00 2013 Pierre Surply
** Last update Sun Feb 23 14:41:27 2014 Pierre Surply
*/

#include "boot.h"
#include "shell.h"
#include "uart.h"
#include "megaboot.h"
#include "utils.h"

#include <avr/pgmspace.h>

#if defined(__AVR_ATmega328P__)
#define RAMSTART        (0x100)
#elif defined(__AVR_ATmega1280__)
#define RAMSTART        (0x200)
#endif

#define BUFF            ((char *)(RAMSTART))

const char pgm_welcome[]      PROGMEM = "*** MegaBoot ***\n\r";
const char pgm_version[]      PROGMEM = "Bootloader version\t";
const char pgm_sign[]         PROGMEM = "Device signature\t";
const char pgm_lfuse[]        PROGMEM = "Low fuse\t\t";
const char pgm_hfuse[]        PROGMEM = "High fuse\t\t";
const char pgm_efuse[]        PROGMEM = "Ext fuse\t\t";
const char pgm_lbits[]        PROGMEM = "Locks bits\t\t";
const char pgm_erase[]        PROGMEM = "Application section erased\n\r";

const char pgm_error[]        PROGMEM = "Error : ";
const char pgm_err_start[]    PROGMEM = "Application section is empty\n\r";

const char pgm_cmd_start[]    PROGMEM = "start";
const char pgm_cmd_info[]     PROGMEM = "info";
const char pgm_cmd_erase[]    PROGMEM = "erase";

const char pgm_nl[]           PROGMEM = "\n\r";

static void prompt(void)
{
        char    c;
        uint8_t i;

        c = 0;
        uart_send('>');
        uart_send(' ');

        for (i = 0; ;++i)
        {
                if ((c = uart_recv()) == '\r')
                {
                        BUFF[i] = 0;
                        uart_send('\n');
                        uart_send('\r');
                        return;
                }
                BUFF[i] = c;
                uart_send(c);
        }
}

void print(const char *s)
{
        strcpy_P((char *)(RAMSTART), s);
        uart_write((char *)(RAMSTART));
}

void print_error(const char *s)
{
        print(pgm_error);
        print(s);
}

void print_entry(const char *e, uint32_t n, uint8_t digits)
{
        print(e);
        utils_utoa(n, BUFF, digits);
        uart_write(BUFF);
        print(pgm_nl);
}


static inline void info(void)
{
        print_entry(pgm_version, 1, 1);
        print_entry(pgm_sign,
                    ((uint32_t) read_sign(SIGN_BYTE1) << 16)        |
                    ((uint32_t) read_sign(SIGN_BYTE2) << 8)         |
                    (uint32_t) read_sign(SIGN_BYTE3), 6);
        print_entry(pgm_lfuse, read_fuse(ADDR_LFUSE), 2);
        print_entry(pgm_hfuse, read_fuse(ADDR_HFUSE), 2);
        print_entry(pgm_efuse, ~(read_fuse(ADDR_EFUSE)), 2);
        print_entry(pgm_lbits, read_fuse(ADDR_LFUSE), 2);
}

void welcome(void)
{
        print(pgm_welcome);
}


void shell(void)
{
        welcome();
        for (;;)
        {
                prompt();
                if (strcmp_P(BUFF, pgm_cmd_info) == 0)
                        info();

                else if (strcmp_P(BUFF, pgm_cmd_start) == 0)
                {
                        if (pgm_read_byte_near(0) == 0xFF)
                                print_error(pgm_err_start);
                        else
                                start_app();
                }
                else if (strcmp_P(BUFF, pgm_cmd_erase) == 0)
                {
                        set_erased();
                        print(pgm_erase);
                }
        }
}
