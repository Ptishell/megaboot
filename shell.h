/*
** shell.h for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Thu Jun 20 17:24:02 2013 Pierre Surply
** Last update Sun Feb 23 14:41:12 2014 Pierre Surply
*/

#ifndef _SHELL_H_
#define _SHELL_H_

void print(const char *s);
void print_error(const char *s);
void welcome(void);
void shell(void);

#endif /* _SHELL_H_ */
