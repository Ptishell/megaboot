/*
** stk500.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Mon Jun 17 10:49:10 2013 Pierre Surply
** Last update Fri Jun 21 18:24:25 2013 Pierre Surply
*/

#include "stk500.h"
#include "megaboot.h"
#include "uart.h"
#include "boot.h"

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <inttypes.h>

static void check_sync(void)
{
        if (uart_recv() != CRC_EOP)
        {
                uart_send(STK_NOSYNC);
                start_app();
        }
        else
                uart_send(STK_INSYNC);
}

static void prog_page(uint16_t addr)
{
        uint8_t         len, i;
        uint8_t         buff[SPM_PAGESIZE];

        uart_seek(1);
        len = uart_recv();
        uart_seek(1);
        for (i = 0; i < len; ++i)
                buff[i] = uart_recv();
        for (; i < SPM_PAGESIZE; ++i)
                buff[i] = 0xFF;
        write_page(buff, addr);
        check_sync();
}

static void read_page(uint16_t addr)
{
        uint16_t        len;
#ifdef __AVR_ATmega1280__
        uint8_t         res;
#endif

        len = uart_recv_word();
        uart_seek(1);
        check_sync();
        do
        {
#ifdef __AVR_ATmega1280__
                asm volatile("elpm %0,Z\n\t":
                             "=r" (res):
                             "z" (addr));
                uart_send(res);
                ++addr;
#else
                uart_send(pgm_read_byte_near(addr++));
#endif
        } while (--len);
}

void universal(void)
{
        uint8_t        data[2];
        uint8_t        i;

        for (i = 0; i < 2; ++i)
                data[i] = uart_recv();

        uart_seek(2);
        check_sync();

        switch ((data[0] << 8) | data[1])
        {
        case UNIV_READ_LFUSE:
                uart_send(read_fuse(ADDR_LFUSE));
                break;
        case UNIV_READ_HFUSE:
                uart_send(read_fuse(ADDR_HFUSE));
                break;
        case UNIV_READ_EFUSE:
                uart_send(read_fuse(ADDR_EFUSE));
                break;
        case UNIV_ERASE:
                set_erased();
        default:
                uart_send(0);
        }
}

void get_param(void)
{
        uint8_t param;

        param = uart_recv();

        check_sync();
        switch (param)
        {
        case STK_SW_MAJOR:
                uart_send(SW_MAJOR);
                break;
        case STK_SW_MINOR:
                uart_send(SW_MINOR);
                break;
        case STK_VTARGET:
                uart_send(VTARGET);
                break;
        case STK_AREF:
                uart_send(AREF);
                break;
        case STK_TOPCARD:
                uart_send(3);
                break;
        default:
                uart_send(0);
        }
}

void stk500_loop(void)
{
        uint16_t        addr;

        addr = 0;
        uart_init();
        for(;;)
        {
                switch (uart_recv())
                {
                case STK_GET_SYNC:
                        check_sync();
                        break;
                case STK_GET_SIGN_ON:
                        check_sync();
                        uart_send('A');
                        uart_send('V');
                        uart_send('R');
                        uart_send(' ');
                        uart_send('S');
                        uart_send('T');
                        uart_send('K');
                        break;
                case STK_SET_PARAMETER:
                        uart_seek(2);
                        check_sync();
                        break;
                case STK_GET_PARAMETER:
                        get_param();
                        break;
                case STK_SET_DEVICE:
                        uart_seek(20);
                        check_sync();
                        break;
                case STK_SET_DEVICE_EXT:
                        uart_seek(4);
                        check_sync();
                        break;
                case STK_LOAD_ADDRESS:
                        addr = uart_recv();
                        addr =  (addr & 0xFF) | (uart_recv() << 8);
#ifdef __AVR_ATmega1280__
                        RAMPZ = addr >> 15;
#endif
                        addr <<= 1;
                        check_sync();
                        break;
                case STK_UNIVERSAL:
                        universal();
                        break;
                case STK_PROG_PAGE:
                        prog_page(addr);
                        break;
                case STK_READ_PAGE:
                        read_page(addr);
                        break;
                case STK_READ_SIGN:
                        check_sync();
                        uart_send(read_sign(SIGN_BYTE1));
                        uart_send(read_sign(SIGN_BYTE2));
                        uart_send(read_sign(SIGN_BYTE3));
                        break;
                default:
                        check_sync();
                }
                uart_send(STK_OK);
        }
}
