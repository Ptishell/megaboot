/*
** uart.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sun Jun 16 09:13:38 2013 Pierre Surply
** Last update Sun Feb 23 14:40:16 2014 Pierre Surply
*/

#include "uart.h"
#include "watchdog.h"

#include <stdlib.h>
#include <avr/io.h>

void uart_init(void)
{
        UCSR0A = (1 << U2X0);
        UCSR0B = (1 << RXEN0) | (1 << TXEN0);
        UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
        UBRR0L = (uint8_t)( (F_CPU + BAUD_RATE * 4L) /
                            (BAUD_RATE * 8L) - 1 );
}

uint8_t uart_recv(void)
{
        while(!(UCSR0A & (1 << RXC0)));
        wdr();
        return UDR0;
}

uint16_t uart_recv_word(void)
{
        uint16_t        w;

        w = uart_recv() << 8;
        w |= uart_recv();

        return w;
}

void uart_seek(size_t i)
{
        for (; i > 0; --i)
                uart_recv();
}

void uart_send(uint8_t c)
{
        while (!(UCSR0A & (1 << UDRE0)));
        UDR0 = c;
}

void uart_write(const char *s)
{
        for (; *s; ++s)
                uart_send(*s);
}
