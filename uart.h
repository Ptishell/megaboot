/*
** uart.h for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sun Jun 16 09:19:15 2013 Pierre Surply
** Last update Sun Feb 23 14:40:25 2014 Pierre Surply
*/

#ifndef _UART_H_
#define _UART_H_

#include <stdlib.h>
#include <inttypes.h>

#define BAUD_RATE       115200L

void            uart_init(void);
uint8_t         uart_recv(void);
uint16_t        uart_recv_word(void);
void            uart_seek(size_t i);
void            uart_send(uint8_t c);
void            uart_write(const char *s);

#endif /* _UART_H_ */
