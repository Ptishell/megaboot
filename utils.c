/*
** utils.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Fri Jun 21 16:58:46 2013 Pierre Surply
** Last update Fri Jun 21 17:12:26 2013 Pierre Surply
*/

#include "utils.h"

#include <inttypes.h>

void utils_utoa(uint32_t number, char *buff, uint8_t digits)
{
        uint8_t d;
        while (digits--)
        {
                d = (number >> (digits * 4)) & 0xF;
                *(buff++) = d >= 10 ? d + 'A' - 10 : d + '0';
        }

        *buff = 0;
}
