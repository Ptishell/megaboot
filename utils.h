/*
** utils.h for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Fri Jun 21 17:04:57 2013 Pierre Surply
** Last update Fri Jun 21 17:08:35 2013 Pierre Surply
*/

#ifndef _UTILS_H_
#define _UTILS_H_

#include <inttypes.h>

void utils_utoa(uint32_t number, char *buff, uint8_t digits);

#endif /* _UTILS_H_ */
