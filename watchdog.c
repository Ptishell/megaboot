/*
** watchdog.c for MegaBoot
**
** Made by Pierre Surply
** <pierre.surply@gmail.com>
**
** Started on  Sat Jun 15 22:14:39 2013 Pierre Surply
** Last update Thu Jun 20 18:28:05 2013 Pierre Surply
*/

#include <avr/io.h>

#include "watchdog.h"

inline void wdr(void)
{
        asm volatile ("wdr\n\t"::);
}

inline void wdc(uint8_t wdtcsr)
{
        WDTCSR |= (1 << WDCE) | (1 << WDE);
        WDTCSR = wdtcsr;
}
